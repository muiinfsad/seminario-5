const express = require('express');
const service = express();

const cartFunctions = require("./CartFunctions.js");

var cart = [];

module.exports = (config) => {
  const log = config.log();
  // Add a request logging middleware in development mode
  if (service.get('env') === 'development') {
    service.use((req, res, next) => {
      log.debug(`${req.method}: ${req.url}`);
      return next();
    });
  }

  service.get('/cart', (req, res) => {
    console.log(cart);
    res.send(cart);
  });

  service.post('/cart/:producto', (req, res) => {
    const { producto } = req.params;
    //console.log(producto);
    cartFunctions.addToCart(producto, cart);
    res.send("Added?");
  });

  service.delete('/cart/:producto', (req, res, next) => {
    const { producto } = req.params;
    cartFunctions.removeFromCart(producto, cart);
    res.send("Deleted?");
  });

  // eslint-disable-next-line no-unused-vars
  service.use((error, req, res, next) => {
    res.status(error.status || 500);
    // Log out the error to the console
    log.error(error);
    return res.json({
      error: {
        message: error.message,
      },
    });
  });
  return service;
};
