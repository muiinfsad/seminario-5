const axios = require('axios');

exports.addToCart = function (itemName, cart) {
	axios.get(`http://localhost:38533/dbContent/${itemName}/`).then((response) => {
        cart.push(response.data);
        console.log("Added");
}, (error) => {console.log("Can't add this item.");});
}

exports.removeFromCart = function (itemName, cart) {
    var removedItem = null;
    for (var itemIndex = 0; itemIndex < cart.length; itemIndex++){
        if (cart[itemIndex].name == itemName){
            cart.splice(itemIndex, 1);
            console.log(cart);
            console.log("Removed");
        }
    }
}  

