var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/stockService");

var stockSchema = new mongoose.Schema({
	name: String,
	price: Number,
	stockNumber: Number
});

var Product = mongoose.model("Product", stockSchema);

// adding a new product to the BD

exports.addNewProduct = function (name, price, stockNumber){
	var product = new Product({
		name: name,
		price: price,
		stockNumber: stockNumber
	});
	product.save(function(err, product){
		if(err){
			console.log("Something is wrong with saving");
		} else {
			console.log("Saved");	
		}
	});
}
	

exports.checkStock = function (itemName) {
	 return new Promise((resolve, reject) => {
		dbPromise(itemName).then((product) => { resolve(product) }, () => {reject();});
	 });
}

dbPromise = function (itemName){
	return new Promise((resolve, reject) => {
		Product.findOne({name: itemName}, function(err, product){
			if(err){
				console.log("Something is wrong with retrieving");
				reject();
			} else {
				console.log("Retrieved");
				console.log(product);
				if(product && product.stockNumber > 0){
					resolve(product);
				} else {
					reject();				
				}
			}
			//mongoose.connection.close()
		});	
	});
}