# Seminario 5

## Actividad 1
El siguiente código se corresponde con la función Login del archivo User.js. A través de una promesa se obtiene si el usuario pertenece al sistema o si las credenciales son inválidas. Se trata de la versión promisificada de la función ya existente de Login.
```javascript
User.prototype.login = function() {
  this.cleanUp()
  return new Promise((resolve, reject) => {
    usersCollection.findOne({username: this.data.username}, (err, attemptedUser) => { 
      if (attemptedUser && attemptedUser.password == this.data.password) {
        resolve("Congrats!");
      } else {
        reject("Invalid username / password.");
      }
    })
  })
}
```
## Actividad 2 
En el desarrollo de esta actividad se reemplazan las funciones callback usadas para el acceso a la base de datos por unas funciones promisificadas. El siguiente código, que se puede encontrar en el fichero **serviceDB/server/dbConnection.js** sirve de ejemplo:
```javascript
exports.checkStock = function (itemName) {
	 return new Promise((resolve, reject) => {
		dbPromise(itemName).then((product) => { resolve(product) }, () => {reject();});
	 });
}

dbPromise = function (itemName){
	return new Promise((resolve, reject) => {
		Product.findOne({name: itemName}, function(err, product){
			if(err){
				console.log("Something is wrong with retrieving");
				reject();
			} else {
				console.log("Retrieved");
				console.log(product);
				if(product && product.stockNumber > 0){
					resolve(product);
				} else {
					reject();				
				}
			}
		});	
	});
```